package es.libreria.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import es.libreria.model.entities.Libro;
import es.libreria.model.service.ILibroService;

import org.springframework.web.bind.annotation.RestController;

/**
 * @author david
 *
 */
@RestController
@RequestMapping(value = "/api/libro")
public class LibroRestController {

	@Autowired
	private ILibroService libroService;

	@GetMapping()
	public List<Libro> getLibros() {
		return libroService.getLibros();
	}

	@DeleteMapping(value = "/{id}")
	public void borrarLibro(@PathVariable final Long id) {
		libroService.borrarLibro(id);
	}

	@GetMapping("/{id}")
	public Libro buscarPorId(@PathVariable final Long id) {
		return libroService.buscarPorId(id);
	}

	@PostMapping
	public Libro nuevo(@RequestBody final Libro libro) {
		return libroService.guardar(libro);
	}

	@PutMapping("/{id}")
	public Libro actualizar(@RequestBody final Libro libro, @PathVariable Long id) {
		// Primero buscamos para comprobar que el id existe
		libroService.buscarPorId(id);
		return libroService.guardar(libro);
	}
	
	@GetMapping("/titulo/{titulo}")
    public List<Libro> buscarPorTitulo(@PathVariable final String titulo) {
        return libroService.buscarPorTitulo(titulo);
    }

}
