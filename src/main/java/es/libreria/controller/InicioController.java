package es.libreria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import es.libreria.model.entities.Libro;
import es.libreria.model.service.ILibroService;

/**
 * @author david
 *
 */
@Controller
public class InicioController {

	@Autowired
	private ILibroService libroService;
	
	@GetMapping("/")
    public String inicio(Model model){
		
		List<Libro> libros = libroService.getLibrosOrderTituloAsc();
		model.addAttribute("libros", libros);
        return "inicio";
    }
}
