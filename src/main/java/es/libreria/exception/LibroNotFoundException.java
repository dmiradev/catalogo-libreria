package es.libreria.exception;

/**
 * @author david
 *
 */
public class LibroNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public LibroNotFoundException(String msg) {
        super(msg);
    }
}
