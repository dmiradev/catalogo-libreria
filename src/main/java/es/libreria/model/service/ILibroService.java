package es.libreria.model.service;

import java.util.List;

import es.libreria.model.entities.Libro;

/**
 * @author david
 *
 */
public interface ILibroService {
	
	List<Libro> getLibros();
	
	List<Libro> getLibrosOrderTituloAsc();
	
	void borrarLibro(final Long id);
	
	Libro buscarPorId(final Long id);

	Libro guardar(final Libro libro);

	List<Libro> buscarPorTitulo(final String titulo);
}
