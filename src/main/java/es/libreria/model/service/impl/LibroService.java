package es.libreria.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.libreria.exception.LibroNotFoundException;
import es.libreria.model.entities.Libro;
import es.libreria.model.repository.ILibroRepository;
import es.libreria.model.service.ILibroService;

/**
 * @author david
 *
 */
@Service
public class LibroService implements ILibroService{

	@Autowired
	private ILibroRepository libroRepository;

	@Override
	public List<Libro> getLibros() {
		return (List<Libro>) libroRepository.findAll();
	}
	
	@Override
	public List<Libro> getLibrosOrderTituloAsc() {
		return libroRepository.findAllByOrderByTituloAsc();
	}

	@Override
	public void borrarLibro(final Long id) {
		libroRepository.findById(id).orElseThrow(() -> new LibroNotFoundException("No existe el libro con id = " + id));
		
		libroRepository.deleteById(id);
		
	}

	@Override
	public Libro buscarPorId(final Long id) {
		 return libroRepository.findById(id)
		          .orElseThrow(() -> new LibroNotFoundException("No existe el libro con id = " + id));
	}

	@Override
	public Libro guardar(final Libro libro) {
		return libroRepository.save(libro);
	}

	@Override
	public List<Libro> buscarPorTitulo(final String titulo) {
		return libroRepository.findByTituloContainingIgnoreCase(titulo);
	}

}
