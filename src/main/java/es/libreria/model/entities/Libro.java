package es.libreria.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author david
 *
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Libro implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String titulo;
	
	@Lob
	private String descripcion;
	
	@Column(nullable = false)
	private String autor;
	
	@Column(nullable = false)
	private String editorial;
	
	@Column(nullable = false)
	private String idioma;
	
	@Column(nullable = false)
	private BigDecimal precio;
	
	@Column(name="fecha_lanzamiento", nullable = false)
	private Date fechaLanzamiento;
	
	@CreatedDate
	@Column(name="fecha_insercion", nullable = false, updatable = false)
	private Date fechaInsercion;
	
	@LastModifiedDate
    @Column(name = "fecha_actualizacion")
    private Date fechaActualizacion;
	
	@Column(nullable = false)
	private Integer stock;
	
	private String foto;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the autor
	 */
	public String getAutor() {
		return autor;
	}

	/**
	 * @param autor the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}

	/**
	 * @return the editorial
	 */
	public String getEditorial() {
		return editorial;
	}

	/**
	 * @param editorial the editorial to set
	 */
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	/**
	 * @return the idioma
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * @param idioma the idioma to set
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	/**
	 * @return the fechaLanzamiento
	 */
	public Date getFechaLanzamiento() {
		return fechaLanzamiento;
	}

	/**
	 * @param fechaLanzamiento the fechaLanzamiento to set
	 */
	public void setFechaLanzamiento(Date fechaLanzamiento) {
		this.fechaLanzamiento = fechaLanzamiento;
	}

	/**
	 * @return the fechaInsercion
	 */
	public Date getFechaInsercion() {
		return fechaInsercion;
	}

	/**
	 * @param fechaInsercion the fechaInsercion to set
	 */
	public void setFechaInsercion(Date fechaInsercion) {
		this.fechaInsercion = fechaInsercion;
	}

	/**
	 * @return the fechaActualizacion
	 */
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	/**
	 * @param fechaActualizacion the fechaActualizacion to set
	 */
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	/**
	 * @return the stock
	 */
	public Integer getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}

	/**
	 * @return the foto
	 */
	public String getFoto() {
		return foto;
	}

	/**
	 * @param foto the foto to set
	 */
	public void setFoto(String foto) {
		this.foto = foto;
	}

	@Override
	public String toString() {
		return "Libro [id=" + id + ", titulo=" + titulo + ", descripcion=" + descripcion + ", autor=" + autor
				+ ", editorial=" + editorial + ", idioma=" + idioma + ", precio=" + precio + ", fechaLanzamiento="
				+ fechaLanzamiento + ", fechaInsercion=" + fechaInsercion + ", fechaActualizacion=" + fechaActualizacion
				+ ", stock=" + stock + ", foto=" + foto + "]";
	}


}
