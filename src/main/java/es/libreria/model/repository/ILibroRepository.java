package es.libreria.model.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import es.libreria.model.entities.Libro;

/**
 * @author david
 *
 */
public interface ILibroRepository extends CrudRepository<Libro, Long> {

	List<Libro> findByTituloContainingIgnoreCase(final String titulo);
	
	List<Libro> findAllByOrderByTituloAsc();

}
