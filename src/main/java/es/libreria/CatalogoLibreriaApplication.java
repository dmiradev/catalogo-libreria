package es.libreria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author david
 *
 */
@SpringBootApplication
@EnableJpaAuditing
public class CatalogoLibreriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatalogoLibreriaApplication.class, args);
	}

}
