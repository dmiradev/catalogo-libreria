package es.libreria;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.MimeTypeUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.libreria.model.entities.Libro;

/**
 * @author david
 *
 */
@AutoConfigureMockMvc
@SpringBootTest
class CatalogoLibreriaApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void contextLoads() {
	}

	
	private Libro libro() throws Exception {

		Libro libro = new Libro();
		libro.setTitulo("La Bestia");
		libro.setDescripcion(
				"<div aria-expanded=\"true\" class=\"a-expander-content a-expander-partial-collapse-content a-expander-content-expanded\" style=\"padding-bottom: 20px;\"> <p>Corre el año 1834 y Madrid, una pequeña ciudad que trata de abrirse paso más allá de las murallas que la rodean, sufre una terrible epidemia de cólera. Pero la peste no es lo único que aterroriza a sus habitantes: en los arrabales aparecen cadáveres desmembrados de niñas que nadie reclama. Todos los rumores apuntan a la Bestia, un ser a quien nadie ha visto pero al que todos temen.</p> <p>Cuando la pequeña Clara desaparece, su hermana Lucía, junto con Donoso, un policía tuerto, y Diego, un periodista buscavidas, inician una frenética cuenta atrás para encontrar a la niña con vida. En su camino tropiezan con fray Braulio, un monje guerrillero, y con un misterioso anillo de oro con dos mazas cruzadas que todo el mundo codicia y por el que algunos están dispuestos a matar.</p> <p><b>De manera magistral, Carmen Mola teje, con los hilos del mejor <i>thriller</i>, esta novela impactante, frenética e implacable, de infierno y oscuridad.</b></p>  </div>");
		libro.setAutor("Carmen Mola");
		libro.setEditorial("Editorial Planeta");
		libro.setIdioma("Español");
		libro.setPrecio(new BigDecimal(21.75));
		libro.setFechaInsercion(new Date());
		libro.setFechaLanzamiento(new Date(1635984000));
		libro.setFoto("https://images-na.ssl-images-amazon.com/images/I/51llUbrJUfL._SX326_BO1,204,203,200_.jpg");
		libro.setStock(15);

		return libro;
		
	}

	@Test
	void getLibros() throws Exception {		

		MvcResult result = mockMvc.perform(get("/api/libro/").accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andReturn();

		List<Libro> libros = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Libro>>() {});
		
		Assertions.assertNotNull(libros);

	}
	
	@Test
	void crearRecuperarYActualizarLibro() throws Exception {		

		//Paso 1: creamos un nuevo libro
		Libro libro = libro();
		
		mockMvc.perform(MockMvcRequestBuilders.post("/api/libro").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(libro)))
				.andExpect(status().isOk());
		
		//Paso2: lo recuperamo usando el id 8 ya que ya existen 7 libros cargados previamente por sql
		MvcResult result = mockMvc.perform(get("/api/libro/8").accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andReturn();

		Libro libroCreado  =objectMapper.readValue(result.getResponse().getContentAsString(), Libro.class);
		
		Assertions.assertEquals(libroCreado.getTitulo(), libro.getTitulo());
		
		//Paso 3: actualizamos el titulo del libro
		libroCreado.setTitulo(libroCreado.getTitulo() + " Modificado");
		
		mockMvc.perform(MockMvcRequestBuilders.put("/api/libro/8").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(libroCreado)))
				.andExpect(status().isOk());
		
		//Paso 4: recuperamos el libro para comprobar que se ha actualizado el titulo
		MvcResult resultActualizacion = mockMvc.perform(get("/api/libro/8").accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andReturn();

		Libro libroActualizado  =objectMapper.readValue(resultActualizacion.getResponse().getContentAsString(), Libro.class);
		
		Assertions.assertNotEquals(libroActualizado.getTitulo(), libro.getTitulo());

	}
	
	@Test
	void borrarLibro() throws Exception {		
		
		mockMvc.perform(delete("/api/libro/1").accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andReturn();

		MvcResult result = mockMvc.perform(get("/api/libro/").accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andReturn();

		List<Libro> libros = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Libro>>() {});
		
		// Comprobamos que no exista el id que acabamos de eliminar
		for(Libro libro : libros) {
			Assertions.assertNotEquals(libro.getId(), Long.valueOf(1));
		}

	}

}
