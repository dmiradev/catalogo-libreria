FROM java:8
ARG JAR_FILE=target/catalogo-libreria-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} catalogo-libreria-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/catalogo-libreria-0.0.1-SNAPSHOT.jar"]
