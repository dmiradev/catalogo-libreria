
# Catalogo Librería
CRUD realizado con Spring Boot para la gestión del catálogo de una librería que permita añadir, actualizar, buscar y eliminar libros del catálogo.

## Name
Catalogo Librería

## Docker
Existe una imagen pública subida al Hub de Docker la cual podemos cargar y ejecutar en nuestro ordenador:
```
docker run -p <puerto_local>:8080  dmira91/catalogo-libreria
```

## Installation
Este proyecto necesita maven para instalar las dependencias.

Una vez descargado el repositorio instalar las dependencias y generar el jar:
```
mvn package
```

**Ejecutar aplicación con el .jar creado**

Si se quiere ejecutar la aplicación, una vez generado el paquete abrir una terminal en la raíz del proyecto y ejecutar (Tener en cuenta la versión del proyecto):
```
java -jar target\catalogo-libreria-0.0.1-SNAPSHOT.jar
```

Ahora la aplicación estará disponible en el puerto 8080 de nuestro ordenador.

**Desplegar aplicación en un contenedor Docker**

Si deseamos ejecutar la aplicación en un contenedor Docker, ejecutar los siguientes comandos en la raíz del proyecto:
```
docker build -t catalogo-libreria .
docker run -p <puerto_local>:8080  catalogo-libreria
```
Ahora la aplicación estará disponible en el puerto local seleccionado.

## Usage
Accediendo al puerto en el que hemos desplegado nuestra aplicación se cargará una ventana con una serie de datos precargados dónde podremos probar los siguientes servicios:

 1. **Búsqueda:** Emplea el endpoint /api/libro (Get) para buscar todos los libros.
 2. **Borrar:** Emplea el endpoint /api/libro/{id} (Delete) para borrar un libro.
 3. **Buscar por id:** Emplea el endpoint /api/libro/{id} (Get) para buscar un libro.
 4. **Crear:**  Emplea el endpoint /api/libro (Post) para crear un nuevo libro.
 5. **Actualizar:** Emplea el endpoint /api/libro/{id} (Put) para actualizar un libro.
 6. **Buscar por titulo:**  Emplea el endpoint /api/libro/titulo/{titulo} (Get) para buscar un libro por su titulo.
